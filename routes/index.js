var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    config: module.parent.exports.set('config')
  });
});

module.exports = router;
