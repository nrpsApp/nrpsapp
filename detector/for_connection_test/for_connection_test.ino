#include <SPI.h>
#include <SFE_CC3000.h>
#include <SFE_CC3000_Client.h>

// Here we define a maximum framelength to 64 bytes. Default is 256.
#define MAX_FRAME_LENGTH 64

// Define how many callback functions you have. Default is 1.
#define CALLBACK_FUNCTIONS 1

// Pins
#define CC3000_INT      2   // Needs to be an interrupt pin (D2/D3)
#define CC3000_EN       7   // Can be any digital pin
#define CC3000_CS       10  // Preferred is pin 10 on Uno

// Connection info data lengths
#define IP_ADDR_LEN     4   // Length of IP address in bytes

// Constants
char ap_ssid[] = "BUFFALO-F7B124";                  // SSID of network
char ap_password[] = "rge6jd7gmcc4t";          // Password of network
//char ap_ssid[] = "BUFFALO-F7B124";
//char ap_password[] = "rge6jd7gmcc4t";
unsigned int ap_security = WLAN_SEC_WPA2; // Security of network
unsigned int timeout = 30000;             // Milliseconds
char server[] = "192.168.1.46";        // Remote host site
String id = "123";
String normal_state = "{\"id\" : " + id + ", \"state\" : \"normal\"}";
String disorder_state = "{\"id\" : " + id + ", \"state\" : \"disorder\"}";
String unknown_state = "{\"id\" : " + id + ", \"state\" : \"unknown\"}";
// Global Variables
SFE_CC3000 wifi =SFE_CC3000(CC3000_INT, CC3000_EN, CC3000_CS);
SFE_CC3000_Client client = SFE_CC3000_Client(wifi);
SFE_CC3000* wifip = &wifi;
SFE_CC3000_Client* clientp = &client;

void setup() {

  ConnectionInfo connection_info;
  int i;

  // Initialize Serial port
  Serial.begin(115200);
  Serial.println();
  Serial.println("---------------------------");
  Serial.println("SparkFun CC3000 - WebClient");
  Serial.println("---------------------------");

  // Initialize CC3000 (configure SPI communications)
  if ( wifi.init() ) {
    Serial.println("CC3000 initialization complete");
  } else {
    Serial.println("Something went wrong during CC3000 init!");
  }

  // Connect using DHCP
  Serial.print("Connecting to SSID: ");
  Serial.println(ap_ssid);
  if(!wifi.connect(ap_ssid, ap_security, ap_password, timeout)) {
    Serial.println("Error: Could not connect to AP");
  }

  // Gather connection details and print IP address
  if ( !wifi.getConnectionInfo(connection_info) ) {
    Serial.println("Error: Could not obtain connection details");
  } else {
    Serial.print("IP Address: ");
    for (i = 0; i < IP_ADDR_LEN; i++) {
      Serial.print(connection_info.ip_address[i]);
      if ( i < IP_ADDR_LEN - 1 ) {
        Serial.print(".");
      }
    }
    Serial.println();
  }

    // Connect to the websocket server
    if (client.connect(server, 3060)) {
      Serial.println("Connected");
    } else {
      Serial.println("Connection failed.");
    }
}
unsigned long befTime = millis()/1000; //loop()ひとつ前の時間 s
unsigned long nowTime = millis()/1000; //現在の時間 s
unsigned long echoTime = 1000000000000; //一ラインメッセージ最終受信時間 s
String string ="";//受信文字列用string
int start = false; //スタートボタン用変数
int rbtn;    //リセットボタン用変数
int eflg;   //エラーフラグ(-1でエラー、0で正常)
unsigned int voltdef; //正常電圧値
unsigned int volt;  //一時電圧値

  void loop(){
   if ( client.available() ) {

     char c = client.read();
     if('\n'==c || '\r' == c){
        //一ライン完了。
        Serial.println(string);
        string = "";
        echoTime = millis()/1000;
     }else{
      string += c;
     }
    }
   if (client.connected()) {
      //ライン受信が５秒間なかった時再接続処理を行う
      if(echoTime + 10 < millis()/1000){
        *wifip = SFE_CC3000(CC3000_INT, CC3000_EN, CC3000_CS);
        *clientp = SFE_CC3000_Client(wifi);
        setup();
        echoTime = 1000000000000;
      }
    
      befTime = nowTime;
      nowTime = millis()/1000;
      //Serial.println(0+nowTime);
      if(digitalRead(8)==LOW){
        start = true;
        digitalWrite(4,LOW);
        inits();
      }
      if(start){
        if(befTime<nowTime){
          volt=input();
          eflg=thinker(voltdef,volt);
          output(eflg);
          Serial.println(eflg);
        }
      }else{
        client.print(unknown_state);
        digitalWrite(4,HIGH);
      }
  }else{
    Serial.println();

    // Close socket
    if ( !client.close() ) {
      Serial.println("Error: Could not close socket");
    }

    // Disconnect WiFi
    if ( !wifi.disconnect() ) {
      Serial.println("Error: Could not disconnect from network");
    }

    // Do nothing
    Serial.println("Finished WebClient test");
    *wifip = SFE_CC3000(CC3000_INT, CC3000_EN, CC3000_CS);
    *clientp = SFE_CC3000_Client(wifi);
    setup();
  }
}
//----------------------------------------------------------------------

unsigned int inits(){    //初期化処理
    int buf;  //計算用一時変数
    long buf2=0;  //計算用変数
    int cnt;  //カウント用変数
    unsigned int voltbuf[30]; //0.1秒毎の電圧値

    digitalWrite(4,HIGH);

     while(1){  //ボタン押されるまでループ

         if(digitalRead(8)==LOW)  //押されたらスタート
         break;

         delay(100);  //0.1秒ループ
     }

    digitalWrite(4,LOW);

    for(cnt=0;cnt<30;cnt++){    //3秒間電圧値をとる
        buf=analogRead(5);  //アナログピン5の値をとる
        voltbuf[cnt]=buf*49;    //電圧値へ変換して記録

        delay(100); //0.1秒待つ
    }

    for(cnt=0;cnt<30;cnt++){  //voltbufをすべて足す
    buf2+=voltbuf[cnt];
  }

  buf2=(buf2)/30; //voltbufの平均値を正常値として記録する
  return buf2;
}

//----------------------------------------------------------------------

unsigned int input(){      //入力読み込み

  int buf;  //計算用一時変数
  unsigned int buf2;  //同上

  buf=analogRead(5);  //アナログ入力端子5ピンの値をbufに

  buf2=buf*49;  //電圧値に変換してvoltに
  return buf2;
}

//------------------------------------------------------------------------

int thinker(unsigned int voltdef,unsigned int volt){  //判断部
  unsigned int buf; //計算用変数
  buf=voltdef*0.95;   //比較値設定項
  if(buf>volt)
    return -1;    //エラーリターン

   return 0;    //正常値リターン
}

//---------------------------------------------------------------------------

void output(int eflg){    //親機への出力
  if(eflg==-1){     //エラー
    digitalWrite(5,HIGH);
                client.print(disorder_state);
                digitalWrite(7,LOW);
        }
  else{       //正常
    digitalWrite(5,LOW);
                digitalWrite(7,HIGH);
                client.print(normal_state);
        //delay(500);

        }
}
