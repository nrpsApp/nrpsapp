'use strict';
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var net = require('net');
var Dao = require('./dao').Dao;
var db = new Dao('localhost', 27017);

var app = express();
var http = require('http').Server(app);
var io = require('socket.io').listen(http);

var routes = require('./routes/index');
var users = require('./routes/users');

//config ファイルを読み込み
var config = JSON.parse(require('fs').readFileSync('config.json'));
//module.parentから参照するするようにする
//module.parent.exports.set('config')
app.set('config', config);

//port番号を設定
http.listen(config.httpSocketIo_port, function() {
    console.log('http and socket.io Server is listening on port ' + config.httpSocketIo_port);
});
app.http = http;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);



/**
 * detectorクラス
 * @constructor
 * @class Detector
 * @param {int} id 一意の値
 */
var Detector = function(id) {
    if (!(this instanceof Detector)) {
        return new Detector(id);
    }

    /**
     * detectorを識別するための一意の値
     * @property id
     * @type int
     */
    this.id = id;

    /**
     * ベット名
     * 誤って同じベット番号を入力されてしまう可能性があるため別にした。
     * @property bet_name
     * @type string
     */
    this.bet_name = "";

    /**
     * 現在のステータス
     * 状態 normal|disorder|instability|unknown|warning
     * @property state
     * @type String
     */
    this.state = 'unknown';

    /**
     * 接続開始時間　オブジェクトが作成された時間 date型ではないので注意
     * @property start_time
     * @type number
     */
    this.start_time　 = (new Date()).getTime();

    /**
     * 前回更新時間された時間
     * @property duration_time
     * @type number
     */
    this.duration_time　 = (new Date()).getTime();
}

/**
 * detectorsManagementクラス
 * @constructor
 * @class detectorsManagement
 */

var DetectorsManagement = function() {
    if (!(this instanceof DetectorsManagement)) {
        return new DetectorsManagement();
    }
    /**
     * detectorオブジェクト配列
     * 現在すべての接続されている検知器を挿入させる。
     * @type array
     */
    this.detectors = [];

    this.io = null;
    this.demo = false;
    this.demoArartId = null;
}

//接続されている機器の監視。
DetectorsManagement.prototype.startDeleteTimeOutdetectors = function() {
    var dm = this;

    var deleteTimeOutdetectors = function() {
        var nowTime = new Date();
        dm.detectors.forEach(function(detector, i) {
            //5秒間~10秒間接続がない時ステートを変更
            if (5000 <= (nowTime.getTime() - detector.duration_time) && (nowTime.getTime() - detector.duration_time) < 10000) {
                if (detector.state !== "instability") {
                    detector.state = "instability";
                    dm.updateRequestFromServer(-1, "", -1); //クライアント更新要求
                }

            }
            //10秒間以上接続がない時現在検知している機器を削除
            if (10000 <= (nowTime.getTime() - detector.duration_time)) {

                if (detector.state !== "unknown") {
                    detector.state = "unknown";
                    //dm.detectors.splice(dm.detectors.indexOf(detector), 1); //削除
                    dm.updateRequestFromServer(3, "検知機との接続が予期せず切断されました。", detector.id); //クライアント更新要求
                    db.recOffline(detector.id,function(err){
                        if(err!=null){
                            //何かをする
                        }
                    });
                }
            }
        });

        //demo
        if (dm.demo) {
            if ((Math.random() * ((100 + 1) - 1)) + 1 <= 1) {
                if (dm.demoArartId == null) {
                    var u = Math.floor((Math.random() * ((10 + 1) - 1)) + 1);
                    dm.demoArartId = u;
                    for (var i = 0; i < 10; i++) {
                        setTimeout(function() {
                            dm.upset(u, "warning");
                        }, i * 1000);
                    }
                    setTimeout(function() {
                        dm.demoArartId = null
                    }, 10 * 1000);
                }
            }
            if ((Math.random() * ((100 + 1) - 1)) + 1 <= 5) {
                if (dm.demoArartId == null) {
                    var a = Math.floor((Math.random() * ((10 + 1) - 1)) + 1);
                    dm.demoArartId = a;
                    for (var i = 0; i < 10; i++) {
                        setTimeout(function() {
                            dm.upset(a, "disorder");
                        }, i * 1000);
                    }
                    setTimeout(function() {
                        dm.demoArartId = null
                    }, 10 * 1000);
                }
            }
            for (var i = 0; i < 10; i++) {
                var b = Math.floor((Math.random() * ((10 + 1) - 1)) + 1);
                if (dm.demoArartId != b) {
                    dm.upset(b, "normal");
                }
            }
        }

        setTimeout(deleteTimeOutdetectors, 1000);
    }
    deleteTimeOutdetectors();
}

/**
 * @method getDetector
 * @param {array}
 * @param {number}
 * 第2引数: 検知器のID
 * 概要: 検知器情報を格納してる配列の中から引数で
 * 指定されたIDと一致するdetectorを返す。
 * 戻り値: 配列の添え字 / detectorがなければ null
 */
DetectorsManagement.prototype.getDetector = function(id) {
    var flg = null;
    this.detectors.forEach(function(detector) {
        if (detector.id === id) {
            flg = detector;
            return true;
        }
    });
    return flg;
}

/**
 * @method delDetector
 * @param {number}
 * 第1引数: 検知器のID
 */
DetectorsManagement.prototype.delDetector = function(id) {
    var gd = this.getDetector(Number(id));
    if (gd != null) {
        var bet_name = this.getDetector(Number(id)).bet_name;
        if (gd.state === "unknown") {
            var x = this.detectors.indexOf(this.getDetector(Number(id)));
            this.detectors.splice(x, 1);
            this.updateRequestFromServer(0, bet_name + "を管理システムから外します。", -1);
            db.recOffline(id,function(err){
                if(err!=null){
                    //何かをする
                }
            });
        } else {
            this.updateRequestFromServer(0, bet_name + "は管理中の検知装置であるため外すことができません。", -1);
        }
    }
}
DetectorsManagement.prototype.setio = function(io) {
    this.io = io;
}

/**
 * クライアント更新要求
 * 以下の場合に入れる。
 * 検知器が接続された時。（オブジェクトを作成）
 * 検知器が切断された時。（オブジェクトを削除）
 * 検知器のステータスが変わった時。
 * クライアントから取得要求を送られた時。
 *
 * @method updateRequestFromServer
 * @param {Number} messageNomber  0:表示しない　1:抜針検知　2:再確認　3:強制切断
 * @param {string} massage1 更新理由　""でも良い
 * @param {Number} detectorId
 */
DetectorsManagement.prototype.updateRequestFromServer = function(messageNomber, message, detectorId) {
    var date = {
        messageNomber: messageNomber,
        message: message,
        detectorId: detectorId,
        serverTime: (new Date()).getTime(),
        detectors: this.detectors
    };
    this.io.sockets.emit("updateRequestFromServer", JSON.stringify(date));
}
/**
 * クライアント初期要求
 *
 * @method updateRequestFromServer
 * @param {Number} messageNomber  0:表示しない　1:抜針検知　2:再確認　3:強制切断
 * @param {string} massage1 更新理由　""でも良い
 * @param {Number} detectorId
 */
DetectorsManagement.prototype.startUpdateRequestFromServer = function(socket) {
    var date = {
        messageNomber: -1,
        message: "",
        detectorId: -1,
        serverTime: (new Date()).getTime(),
        detectors: this.detectors
    };
    socket.emit("updateRequestFromServer", JSON.stringify(date));
}


/**
 * アップセット
 *
 * @method upset
 * @param {number}
 * @param {string}
 */
DetectorsManagement.prototype.upset = function(id, state) {
        var dm = this;
        //新規検知機
        if (this.getDetector(id) === null) {
            var detector = new Detector(id);
            detector.state = state;
            detector.bet_name = "" + id + "番"; //ここにデータベースから最新のなまえを取得し挿入
            this.detectors.push(detector);
            this.updateRequestFromServer(0, "検知器が接続されました。", detector.id);
            db.addDoc(id, function(err) {
                if (err != null) {
                    //何かをする。
                }
                db.recOnline(id, function(err) {
                    if (err != null) {
                        //何かをする。
                    }
                });
            });

        } else {
            var detector = this.getDetector(id);
            //ステータスが変化していたときに更新要求をする。
            if (detector.state != state) {
                //再接続をしたか判断
                if (detector.state === "unknown") {
                    detector.state = state;
                    detector.duration_time = (new Date()).getTime();

                    this.updateRequestFromServer(0, "検知器が再接続されました。", detector.id);
                    db.recOnline(id, function(err) {
                        if (err != null) {
                            //何かをする。
                        }
                    });
                } else {
                    detector.state = state;
                    detector.duration_time = (new Date()).getTime();
                    switch (state) {
                        case "disorder":
                            this.updateRequestFromServer(1, "抜針を検知しました。", detector.id);
                            db.pushDisorder(detector.id, function(err,result) {
                                if (err != null) {
                                    //何かをする。
                                    console.log(err);
                                }
                                db.getAllLoggedStateDocs(function(docs) {
                                    // console.log("docs:  ", docs);
                                    dm.io.sockets.emit("stateLog", docs);
                                    //db.close();
                                });
                            });

                            break;
                        case "warning":
                            this.updateRequestFromServer(2, "電極の装着状態を再確認し、検知機を再起動してください。", detector.id);
                            break;
                        default:
                            this.updateRequestFromServer(-1, "", -1);
                    }
                }
            } else {
                detector.state = state;
                detector.duration_time = (new Date()).getTime();
            }
        }
    }
    /**
     * demoモード切り替え
     *
     * @method demoSwitch
     */
DetectorsManagement.prototype.demoSwitch = function() {
    if (this.demo) {
        this.demo = false;
    } else {
        this.demo = true;
    }
}



var dm = new DetectorsManagement();
dm.setio(io);
dm.startDeleteTimeOutdetectors();
//TCP サーバー
net.createServer(function(tcp_socket) {
    tcp_socket.on('data', function(data) {

        //パースできなかったとき処理を終了する
        try {
            var json = JSON.parse(data);
        } catch (e) {
            return;
        }
        //アップセットさせる。
        if (json.state == "treatfin") {
            dm.upset(json.id, "unknown");
            dm.updateRequestFromServer(0, "正常に治療終了しました", json.id);
            db.recOffline(json.id, function(err) {
                if (err != null) {
                    //何かをする。,
                }
            });
        } else {
            dm.upset(json.id, json.state);
        }


        //受信されたデータをエコーバックする。
        //tcp_socket.write(data+"\n");
    });

    tcp_socket.on('end', function() {
        console.log('end');
    });

    tcp_socket.on('close', function() {
        console.log('close');
    });

    tcp_socket.on('error', function(e) {
        console.log('error ', e);
    });

}).listen(config.tcp_port, function() {
    console.log('TCP Server is listening on port 3060');
});


//io.onへこれらを挿入
io.on('connection', function(socket) {
    //クライアントから更新要求イベントを設定
    socket.on("acquisitionRequestFromClient", function(data) {
        dm.startUpdateRequestFromServer(socket);
    });
    socket.on("deletegridFromClient", function(id) {
        dm.delDetector(id);
    });
    //demo
    socket.on("demo", function(data) {
        dm.demoSwitch();
    });

    //(テスト用に配置) detectorのstateにログが書き込まれたら実行してください
    db.getAllLoggedStateDocs(function(docs) {
        // console.log("docs:  ", docs);
        socket.emit("stateLog", docs);
        //db.close();
    });

});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
module.exports = app;
