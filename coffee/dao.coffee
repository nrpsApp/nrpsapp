assert = require('assert');
mongo = require('mongodb')
Db = mongo.Db
Server = mongo.Server
# MongoClient = mongo.MongoClient
# ReplSetServers = mongo.ReplSetServers
# ObjectID = mongo.ObjectID
# Binary = mongo.Binary
# GrindStore = mongo.GridStore
# Grind = mongo.Grid
# Code = mongo.Code
# BSON = mongo.BSON

# https://docs.mongodb.org/manual/reference/operator/
# http://mongodb.github.io/node-mongodb-native/api-generated/
exports.Dao = class Dao
    conect = null
    database = null

    #DBへ接続します。
    #nrpsAppというDBがなければ作成されます
    # @param [String] ip DBのIPアドレスを指定
    # @param [Number] port DBのポートを指定
    # @example
    #   var Dao = require('./dao').Dao;
    #   var db = new Dao('localhost', 27017);
    constructor: (ip, port) ->
        _database = new Db('nrpsApp', new Server(ip, port))
        _database.open (err, db)->

            if not err
                database = db
                console.log "connect mongoDB Server of port " + port
                conect = yes
                collection = db.collection('bed')
                collection.findOne {}, (err, item) ->
                    if item is null
                        collection.createIndex {id: 1}, {unique: true} #IDユニーク制約をつける
                        console.log "created index"
            else
                conect = no

    #指定されたIDを持つドキュメント(RDBでいうレコード)を追加します。
    #追加する際はドキュメントのIDに重複がないかもチェックされます。
    # @param [Number] id 検知器のID
    # @param [callback] func 第1引数エラーメッセージをを受け取ります。正常ではnull。逐次実行したい場合はこの中に処理を記入してください。
    # @example
    addDoc: (id, func) ->
        if conect is yes
            console.log "conect"
        if conect is yes
                collection = database.collection('bed')
                collection.insert({ #idだけ持つドキュメントを作る
                    "id": id,
                    "name": [],
                    "access_log": [],
                    "state": {
                        "disorder": [],
                        "instability": [],
                        "unknown": []
                    }
                }, (err, result) ->
                    func err
                )


    #指定されたIDを持つドキュメント(RDBでいうレコード)を削除します。
    # @param [Number] id 検知器のID
    # @param [callback] func 第1引数エラーメッセージをを受け取ります。正常ではnull。逐次実行したい場合はこの中に処理を記入してください。
    delDoc: (id, func) ->
        if conect is yes
            collection = database.collection('bed')
            collection.remove {"id": id}, (err, result) ->
                console.log "result.result.n  :  ", result.result.n
                console.log "err:  ", err
                if result.result.n is 0
                    err = "指定されたデータを削除できませんでした。指定したIDを確認してください"
                func err


    #指定されたIDを持つドキュメントに、
    #検知器がオンラインになった時刻を記録します。
    # @param [Number] id 検知器のID
    # @param [callback] func 第1引数エラーメッセージをを受け取ります。正常ではnull。逐次実行したい場合はこの中に処理を記入してください。
    recOnline: (id, func) ->
        if conect is yes
            collection = database.collection('bed')
            collection.findOne {"id": id}, (err, item) ->
                if item isnt null
                    if item.access_log.length is 0 #access_logが空の場合はonlineのみ記録したオブジェクトを挿入
                        collection.update {"id": id}, {$push: "access_log": {"online": (new Date).getTime(), "offline": -1} }, (err, result) ->
                            func err
                    else
                        if item.access_log[item.access_log.length-1].offline isnt -1
                            collection.update {"id": id}, {$push: "access_log": {"online": (new Date).getTime(), "offline": -1} }, (err, result) ->
                                func err
                        else
                            func "offlineになった時刻が記録されていない可能性があります。"
                else
                    func "更新対象データが見つかりません。指定したIDを確認してください。"


    #指定されたIDを持つドキュメントに、
    #検知器がオフラインになった時刻を記録します。
    # @param [Number] id 検知器のID
    # @param [callback] func
    recOffline: (id, func) ->
        if conect is yes
            collection = database.collection('bed')
            collection.findOne {"id": id}, (err, item) ->
                if item isnt null
                    tail = item.access_log.length-1
                    if item.access_log[tail].offline is -1
                        collection.update {"id": id},{$set: "access_log.#{tail}.offline": (new Date).getTime()}, (err, result) ->
                    else
                        func "すでにオフラインになった時刻を記録してあります。"
                else
                    func "IDの指定を間違っている可能性があります"

    #指定されたIDを持つドキュメントに、検知を記録
    # @param [Number] id 検知器のID
    # @param [callback] func 第1引数: エラーを受け取ります
    pushDisorder: (id, func) ->
        if conect is yes
            collection = database.collection('bed')
            collection.findOne {"id": id}, (err, item) ->
                if item isnt null and not err
                    console.log item
                    stateObj = item.state
                    stateObj["disorder"].push (new Date).getTime()
                    collection.update {"id": id},{$set: {"state": stateObj}}, (err, result) ->
                        func err, result


    #抜針状態が記録されているすべてのオブジェクトを取得します
    # @param [callback] func 第1引数: err,  第2引数: item 最後にnullが出力される
    getDisorderObj: (func) ->
        if conect is yes
            collection = database.collection('bed')
            collection.find({ "state.disorder": { $exists: true, $not: {$size: 0} } }, {"_id": 0}).toArray (err, docs) ->
                if docs isnt null
                    func docs

    # 以下はカーソルでのアクセス
    # getDisorderObj: (func) ->
    #     @database.open (err, db) ->
    #         cursor = @collection.find { "state.disorder": { $exists: true, $not: {$size: 0} } }, {"_id": 0}
    #         cursor.each (err, item) ->
    #             if item isnt null
    #                 func err, item

    #stateに何らかのデータが記録されているすべてのオブジェクトを取得します
    # @param [callback] func 第1引数: docs 配列を受け取ります
    getAllLoggedStateDocs: (func) ->
        if conect is yes
            collection = database.collection('bed')
            collection.find({ $or: [{"state.disorder": { $exists: true, $not: {$size: 0} }},
            {"state.instability": { $exists: true, $not: {$size: 0} }},
            {"state.unknown": { $exists: true, $not: {$size: 0} }}, ]}, {"_id": 0}).toArray (err, docs) ->
                if docs isnt null
                    func docs



    #指定されたIDを持つドキュメントに、
    #患者名を登録します。その際に登録された時刻も記録します
    # @param [Number] id 検知器のID
    # @param [String] strName 患者の名前
    # @param [callback] func 第1引数: エラーを受け取ります
    setName: (id, strName, func) ->
        if conect is yes
            collection = database.collection('bed')
            collection.findOne {"id": id}, (err, item) ->
                if item isnt null
                    collection.update {"id": id}, {$set: "name": {"setting_date": (new Date).getTime(), "patient": strName} }, (err, result) ->
                        func err, result


    # #患者名にヒットするIDを求める
    # # @param [String] strName 患者名
    # # @return [Array<Number>]  患者名に該当するIDを配列で返す
    # nameToId: (strName) ->
    #     return id
    #
    # #指定されたIDを持つ検知器の稼働時間を求めます。
    # # @param [Number] id 検知器のID
    # # @return [Number] 稼働時間を1970年間からの経過ミリ秒で返す
    # getDuration: (id) ->
    #     return intDuration

    close: () ->
        database.close
        console.log "db was closed"
