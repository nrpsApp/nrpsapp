'use strict';

var gulp = require('gulp');
var coffee = require('gulp-coffee');
var browserSync = require('browser-sync');
var nodemon = require('gulp-nodemon');
var plumber = require("gulp-plumber");

gulp.task('default', ['browser-sync', 'watch'], function () {
});

gulp.task('browser-sync', ['nodemon'], function() {
	browserSync.init(null, {
		proxy: "http://localhost:8000",
		files: ["public/**/*.*", "views/**/*.*"],
		browser: "google chrome",
		port: 3000,
	});
});

gulp.task('nodemon', function (cb) {
	var started = false;
	return nodemon({
		script: 'app.js'
	}).on('start', function () {
		// to avoid nodemon being started multiple times
		// thanks @matthisk
		if (!started) {
			cb();
			started = true;
		}
	});
});

gulp.task('compile-coffee', function() {
	return gulp.src('./coffee/**/*.coffee')
	.pipe(plumber())
	.pipe(coffee())
	.pipe(gulp.dest('./'));
});

gulp.task('watch', function(){
	gulp.watch('./coffee/*.coffee', ['compile-coffee']);
});
