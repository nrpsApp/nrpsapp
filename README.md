# README
##アプリケーション実行環境

* [node.js](https://nodejs.org/en/) アプリケーション実行環境(必須)

* [mongodb](https://www.mongodb.org/)　ログ機能用(推奨)
##ポート設定
config.jsonにてウェブアプリケーション（推奨80port)と検知装置からのポート番号を設定できます。

##最初に行うこと
bin,app.js等があるフォルダを作業フォルダにする。
DB用のフォルダを作成
```
#!shell
mkdir dump
```
アプリケーションを実行する上で必要なモジュールを取得する
```
#!shell
npm install
```
##実行方法
###アプリケーション＋DB実行方法
```
#!shell
mongod --dbpath ./dump/ &
npm start
```
###アプリケーションのみ
```
#!shell
npm start
```